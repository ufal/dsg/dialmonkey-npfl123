#!/usr/bin/env python3

import sys
from ..component import Component


class ConsoleOutput(Component):
    """Print the output to the console, following a 'SYSTEM:' prompt, one utterance per line."""

    def __init__(self, *args):
        super(ConsoleOutput, self).__init__()

    def __call__(self, utterance, *args, **kwargs):
        print('SYSTEM:', utterance)


class FileOutput(Component):
    """Print output to the given file (default to stdout), one utterance per line."""

    def __init__(self, config, *args):
        super(FileOutput, self).__init__(config)
        self.output_fd = sys.stdout
        self.encoding = config.get('output_encoding', 'UTF-8')
        if config and config.get('output_file', '') not in ['', '-']:
            self.output_fd = open(self.config['output_file'], 'wt', encoding=self.encoding)

    def __call__(self, utterance, *args, **kwargs):
        print(utterance, file=self.output_fd)

    def __del__(self):
        self.output_fd.close()


class DialogueLogOutput(FileOutput):
    """Print user-system logs of the dialogues (prefixed by U: and S:)."""

    def __init__(self, config, *args):
        super(DialogueLogOutput, self).__init__(config, *args)

    def __call__(self, utterance, dial, *args, **kwargs):
        if utterance == "<EMPTY>" and dial.eod:  # don't print <EMPTY> at the end of a dialog, we know it's the end of the dialogue
            return
        # log back user utterance, which is in history already at this point
        print("U: " + dial.history[-1]['user'], file=self.output_fd)
        # make any newlines in system utterance shifted slightly for continuation
        utterance = utterance.replace("\n", "\n  ")
        print("S: " + utterance, file=self.output_fd)

    def reset(self):
        """Print empty line at the end of the dialogue."""
        print("", file=self.output_fd)
