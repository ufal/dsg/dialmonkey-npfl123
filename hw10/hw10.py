from wav2vec import DataCollatorCTCWithPadding, load_dataset, train_on_data
import os
from transformers import Wav2Vec2Processor, Wav2Vec2ForCTC
import torch

# Finish this code using stuff from wav2vec
# And take inspiration from
# - https://huggingface.co/facebook/wav2vec2-base-960h
# - https://colab.research.google.com/github/patrickvonplaten/notebooks/blob/master/Fine_tuning_Wav2Vec2_for_English_ASR.ipynb


# First, some handy methods (to be completed by you)

def predict(model, processor, data):
    """Get predictions for the data with the given model & processor."""
    # Implement this
    pass

def load_data_from_dir(data_dir):
    """Load data from the given directory, use file names to get the transcriptions."""
    # Use this transcription map -- note they are upper case!
    transcription_map = {
        0: 'ZERO',
        1: 'ONE',
        2: 'TWO',
        3: 'THREE',
        4: 'FOUR',
        5: 'FIVE',
        6: 'SIX',
        7: 'SEVEN',
        8: 'EIGHT',
        9: 'NINE',
    }
    pass


# 1. Load the base (not finetuned) model + processor + data collator

processor =  # XXX
model = # XXX
model.config.update({"mask_time_length": 8})  # (some of our files are too short, so we shorten masking/dropout size)
collator = DataCollatorCTCWithPadding(processor=processor, padding=True)


# 2. Load the data for random split

# 3. Decode with base model, measure WER

# 4. Train the model + decode + measure again

# 5. Reload the model from scratch, load data for per-speaker split, train again

# 6. Decode, measure WER & find most frequently misheard digits

