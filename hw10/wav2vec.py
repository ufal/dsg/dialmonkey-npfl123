
from transformers import Wav2Vec2Processor
import librosa
import torch
import numpy as np
from dataclasses import dataclass
import evaluate
from typing import Dict, List, Optional, Union
from transformers import TrainingArguments
from transformers import Trainer


wer_metric = evaluate.load("wer")


@dataclass
class DataCollatorCTCWithPadding:
    """
    Data collator serves for padding all data in the batch to the same length.
    Args:
        processor (:class:`~transformers.Wav2Vec2Processor`)
            The processor used for proccessing the data.
        padding (:obj:`bool`, :obj:`str` or :class:`~transformers.tokenization_utils_base.PaddingStrategy`, `optional`, defaults to :obj:`True`):
            Select a strategy to pad the returned sequences (according to the model's padding side and padding index)
            among:
            * :obj:`True` or :obj:`'longest'`: Pad to the longest sequence in the batch (or no padding if only a single
              sequence if provided).
            * :obj:`'max_length'`: Pad to a maximum length specified with the argument :obj:`max_length` or to the
              maximum acceptable input length for the model if that argument is not provided.
            * :obj:`False` or :obj:`'do_not_pad'` (default): No padding (i.e., can output a batch with sequences of
              different lengths).
        max_length (:obj:`int`, `optional`):
            Maximum length of the ``input_values`` of the returned list and optionally padding length (see above).
        max_length_labels (:obj:`int`, `optional`):
            Maximum length of the ``labels`` returned list and optionally padding length (see above).
        pad_to_multiple_of (:obj:`int`, `optional`):
            If set will pad the sequence to a multiple of the provided value.
            This is especially useful to enable the use of Tensor Cores on NVIDIA hardware with compute capability >=
            7.5 (Volta).
    """

    processor: Wav2Vec2Processor
    padding: Union[bool, str] = True
    max_length: Optional[int] = None
    max_length_labels: Optional[int] = None
    pad_to_multiple_of: Optional[int] = None
    pad_to_multiple_of_labels: Optional[int] = None

    def __call__(self, features: List[Dict[str, Union[List[int], torch.Tensor]]]) -> Dict[str, torch.Tensor]:
        # split inputs and labels since they have to be of different lenghts and need
        # different padding methods
        input_features = [{"input_values": feature["input_values"]} for feature in features]
        label_features = [{"input_ids": feature["labels"]} for feature in features]

        batch = self.processor.pad(
            input_features,
            padding=self.padding,
            max_length=self.max_length,
            pad_to_multiple_of=self.pad_to_multiple_of,
            return_tensors="pt",
        )
        with self.processor.as_target_processor():
            labels_batch = self.processor.pad(
                label_features,
                padding=self.padding,
                max_length=self.max_length_labels,
                pad_to_multiple_of=self.pad_to_multiple_of_labels,
                return_tensors="pt",
            )

        # replace padding with -100 to ignore loss correctly
        labels = labels_batch["input_ids"].masked_fill(labels_batch.attention_mask.ne(1), -100)

        batch["labels"] = labels

        return batch


def compute_metrics(processor, model_preds):
    """Compute WER on the given model predictions with the given processor. Used during training.
    Note that what you'll get by calling model() is different from what this function expects!"""
    pred_logits = model_preds.predictions
    pred_ids = np.argmax(pred_logits, axis=-1)

    model_preds.label_ids[model_preds.label_ids == -100] = processor.tokenizer.pad_token_id

    pred_str = processor.batch_decode(pred_ids)
    # we do not want to group tokens when computing the metrics
    label_str = processor.batch_decode(model_preds.label_ids, group_tokens=False)

    wer = wer_metric.compute(predictions=pred_str, references=label_str)

    return {"wer": wer}


def load_dataset(processor, audio_files, transcripts):
    """Load data from the given files, with appropriate transcripts.
    Uses the given processor, assumes a sampling rate of 16k (since that's what wav2vec2 was trained with)"""

    data = []
    for af, tr in zip(audio_files, transcripts):
        audio, _ = librosa.load(af, sr=16000)
        input_values = processor(audio, return_tensors="pt", padding="longest", sampling_rate=16000).input_values[0]
        with processor.as_target_processor():
            char_ids = processor(tr).input_ids
        data.append({'input_values': input_values,
                     'input_length': len(input_values),
                     'labels': char_ids})
    return data


def train_on_data(model, processor, collator, output_dir, train_data, eval_data):
    """Finetunes the given model (with the given data processor & data collator) on the given data.
    Uses output_dir to save checkpoints. Training parameters are set up so it works with the FSDD data
    (feel free to experiment with it)."""

    training_args = TrainingArguments(
        output_dir=output_dir,
        group_by_length=True,
        per_device_train_batch_size=16,
        evaluation_strategy="epoch",
        num_train_epochs=10,
        fp16=True,
        gradient_checkpointing=True,
        logging_steps=20,
        learning_rate=1e-4,
        weight_decay=0.005,
        warmup_steps=500,
        save_total_limit=1,
        save_strategy="epoch",
        push_to_hub=False,
    )

    trainer = Trainer(
        model=model,
        data_collator=collator,
        args=training_args,
        compute_metrics=lambda pred: compute_metrics(processor, pred),
        train_dataset=train_data,
        eval_dataset=eval_data,
        tokenizer=processor.feature_extractor,
    )
    trainer.train()
