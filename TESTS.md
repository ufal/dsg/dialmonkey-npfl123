# NPFL123 Labs Tests

Runs some basic sanity checks for homework assignments.
Use with the same conda/virtualenv as your Dialmonkey.
Assumes checking in the current directory, assumes you have the correct branches set up.

For instance, to check `hw1`, run:

```
./run_tests.py hw1
```

If you also want to check that your merge request & branch are set up correctly,
run with the `--check-git` parameter, but note that this may delete your uncommitted changes!

Always update before running this, we're fixing/updating tests for new assignments as we go.
Some may only be available at the last minute, we're sorry for that.
