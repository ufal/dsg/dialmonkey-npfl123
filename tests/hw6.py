
from logzero import logger
from dialmonkey.conversation_handler import ConversationHandler
from dialmonkey.utils import load_conf
from dialmonkey.input.text import ListInput
from dialmonkey.da import DA
from dialmonkey.dialogue import Dialogue
from .common import find_component
from .hw3 import load_nlu_examples


DEADLINE = '2025-05-09'
FILES = ['hw6/outputs.txt',
         ('dialmonkey/policy/rule_[a-zA-Z_0-9-]+.py', 1),
         (r'conf/act_[a-zA-Z_0-9-]+.yaml', 1)]


def check(files):

    errors = 0
    string_mode = False

    for pattern, matches in files.items():

        if pattern.startswith('conf'):
            try:
                conf = load_conf(matches[0], remove_output_setting=True)
            except Exception as e:
                logger.error(f'Cannot load config, aborting: {e}')
                return errors + 1

            if not find_component(conf, r'dialmonkey\.policy\.rule'):
                logger.error(f'Didn\'t find the policy in config file.')
                errors += 1

        if pattern == 'hw6/outputs.txt':
            outputs = open(matches[0], mode='r', encoding='UTF-8').readlines()
            if len(outputs) < 15:
                logger.warning(f'File {matches[0]} seems too short (<15 lines)')
                errors += 1
            outputs = [l.strip() for l in outputs if l.strip()]  # skip empty lines

            try:
                outputs = [DA.parse_cambridge_da(l) if '&' not in l and ',' in l else DA.parse(l)
                           for l in outputs]
                for a in outputs:
                    a.sort()
            except Exception as e:
                string_mode = True
                logger.warning(f'Cannot parse dialogues properly (likely due to quote characters), will continue in string mode.')
                errors += 1

    _, nlu_examples = load_nlu_examples('hw3/examples.tsv')

    # run dialmonkey with the given config file
    input_feed = ListInput()
    handler = ConversationHandler(conf, logger, user_stream=input_feed)
    for (text, _), ref_action in zip(nlu_examples, outputs):

        if not string_mode:
            ref_action.sort()

        # run the pipeline
        input_feed.set_inputs([text])
        dial = handler.run_dialogue(Dialogue())
        pred_action = dial.history[-1]['action']
        if not string_mode:
            try:
                pred_action = DA.parse_cambridge_da(pred_action)
                pred_action.sort()
            except Exception as e:
                logger.warning(f'Cannot parse output properly (likely due to quote characters).')
                ref_action = str(ref_action)
                errors += 1

        # check equality
        if pred_action != ref_action:
            logger.warning(f'Unexpected action for `{text}\':\n\texpected {ref_action}\n\tgot      {pred_action}.')
            errors += 1

    return errors
