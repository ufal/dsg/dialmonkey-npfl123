
import os
from logzero import logger

DEADLINE = '2025-05-26'
FILES = ['hw10/README.md',
         ('hw10/hw10.(py|ipynb)', 2),]


def check(files):

    errors = 0

    for pattern, matches in files.items():
        if pattern.startswith('hw10/README'):
            if os.path.getsize(matches[0]) < 200:
                logger.warning(f'File {pattern} is too small (<200 bytes).')
                errors += 1
        elif pattern.startswith('hw10/hw10'):
            changed_size = False
            for fname in matches:
                if ((fname.endswith('.py') and os.path.getsize(fname) > 1596) or
                        (fname.endswith('.ipynb') and os.path.getsize(fname) > 5832)):
                    logger.info(f'File {fname} seems to have been changed.')
                    changed_size = True
            if not changed_size:
                logger.warning('Neither code file (py/ipynb) seems to have changed from the template.')
                errors += 1

    return errors
