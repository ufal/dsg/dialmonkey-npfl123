
import os
from logzero import logger

DEADLINE = '2025-03-10'
FILES = ['hw1/README.md',
         ('hw1/examples-[a-zA-Z_0-9-]+.txt', 2),
         ('hw1/flowchart-[a-zA-Z_0-9-]+.(jpg|jpeg|png|pdf)', 2)]


def check(files):

    errors = 0

    for pattern, matches in files.items():
        if pattern.startswith('hw1/README'):
            if os.path.getsize(matches[0]) < 100:
                logger.warning(f'File {pattern} is too small (<100 bytes).')
                errors += 1
        elif pattern.startswith('hw1/examples'):
            for fname in matches:
                num_lines = sum(1 for line in open(fname, mode='r', encoding='UTF-8'))
                if num_lines < 25:
                    logger.warning(f'File {fname} seems too short (<25 lines)')
                    errors += 1
        elif pattern.startswith('hw1/flowchart'):
            for fname in matches:
                if os.path.getsize(matches[0]) == 0:
                    logger.warning(f'File {fname} is empty.')
                    errors += 1

    return errors
