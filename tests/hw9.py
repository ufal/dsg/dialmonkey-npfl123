
from logzero import logger
from .common import check_file_presence
from telegram import Update, Message, Chat, Bot
from telegram.ext import Application, CallbackContext
import os
import datetime
import traceback
import asyncio


DEADLINE = '2025-05-22'
FILES = ['hw9/server.py',
         (r'hw9/README(\.md|\.txt)?', 1)]
FB_TEST_REQUEST = {
    'object': 'page',
    'entry': [
        {
            'messaging': [
                {
                    'message': 'Hello.',
                    'sender': {'id': '1234'},
                    'recipient': {'id': '2345'},
                }
            ],
        }
    ],
}
GOOGLE_TEST_REQUEST = {
    'queryResult': {
        'intent': {
            'displayName': 'Default Fallback Intent'
        },
        'queryText': 'Hello.',
        'parameters': {},
    },
    'session': '/sessions/',
    'originalDetectIntentRequest': {},
}


class PseudoBot(Bot):

    def __init__(self):
        self._last_message = None
        super().__init__(token='XXX')

    async def send_message(self, *args, **kwargs):
        self._last_message = kwargs['text']


def check(files):

    errors = 0

    for pattern, matches in files.items():

        if pattern.startswith('hw9/server'):
            try:
                from hw9 import server
            except Exception as e:
                traceback.print_exc()
                logger.error(f'Cannot import the server module, aborting: {str(e)}')
                return errors + 1

            if hasattr(server, 'reply_telegram'):
                logger.info('Implementation: telegram')
                try:
                    # creating pseudo-telegram context that will hold the reply
                    pseudo_bot = PseudoBot()
                    tapp = Application.builder().bot(pseudo_bot).build()
                    update = Update(update_id=1,
                                    message=Message(message_id=1,
                                                    chat=Chat(id=123, type='PRIVATE'),
                                                    text='Hello.',
                                                    date=datetime.datetime.now()))
                    cc = CallbackContext(tapp, chat_id=123)
                    update.message._bot = pseudo_bot
                    loop = asyncio.get_event_loop()
                    cr = server.reply_telegram(update, cc)
                    loop.run_until_complete(cr)
                    assert isinstance(pseudo_bot._last_message, str)
                    logger.info('Reply: %s' % pseudo_bot._last_message)
                except Exception as e:
                    traceback.print_exc()
                    logger.error(f'Reply function does not work as expected: {str(e)}')
                    errors += 1

            elif hasattr(server, 'reply_facebook'):
                logger.info('Implementation: facebook')
                try:
                    with server.app.test_request_context('/', method='POST', json=FB_TEST_REQUEST):
                        server.reply_facebook()  # will fail sending, so not checking for OK, just shouldn't crash
                except Exception as e:
                    traceback.print_exc()
                    logger.error(f'Reply function does not work as expected: {str(e)}')
                    errors += 1

            elif hasattr(server, 'reply_alexa'):
                logger.info('Implementation: alexa')
                file_errs, _ = check_file_presence('hw9', ['intent_schema.json'])
                errors += file_errs
                try:
                    resp = server.reply_alexa('Hello')
                    assert resp is not None  # won't bother with importing flask_ask stuff
                except Exception as e:
                    traceback.print_exc()
                    logger.error(f'Reply function does not work as expected: {str(e)}')
                    errors += 1

            elif hasattr(server, 'reply_google'):
                logger.info('Implementation: google')
                from flask import Flask
                from flask_assistant import Assistant

                file_errs, _ = check_file_presence('hw9', ['agent.json', 'package.json'])
                errors += file_errs
                try:
                    app = [getattr(server, x) for x in dir(server)
                           if isinstance(getattr(server, x), (Flask, Assistant))][0]
                    if isinstance(app, Assistant):
                        app = app.app
                    path = [rule.rule for rule in app.url_map.iter_rules() if rule.endpoint == '_flask_assitant_view_func'][0]
                    client = app.test_client()
                    resp = client.post(path, json=GOOGLE_TEST_REQUEST)
                    assert resp.status.startswith('200')
                except Exception as e:
                    traceback.print_exc()
                    logger.error(f'Implementation does not work as expected: {str(e)}')
                    errors += 1

            else:
                logger.error(f'No known reply function implemented.')
                errors += 1

        elif pattern.startswith('hw9/README'):
            if os.path.getsize(matches[0]) < 100:
                logger.warning(f'File {matches[0]} is too small (<100 bytes).')
                errors += 1

    return errors
