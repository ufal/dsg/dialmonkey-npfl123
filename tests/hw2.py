
import os
import json
from logzero import logger

DEADLINE = '2025-03-17'
FILES = ['hw2/stats.py',
         'hw2/results.json',
         'hw2/stats.md']

REF_RESULTS = {
    "task5_user": {
        "dialogues_total": (1000, 1000),
        "turns_total": (12936, 12936),
        "mean_dialogue_turns": (12.936, 12.936),
        "stddev_dialogue_turns": (1.60, 1.70),
        "words_total": (61000, 64000),
        "mean_dialogue_words_per_turn": (4, 6),
        "stddev_dialogue_words_per_turn": (3, 4),
        "vocab_size": (80, 90),
        "entropy": (4, 8),
        "cond_entropy": (0.8, 2),
    },
    "task5_system": {
        "dialogues_total": (1000, 1000),
        "turns_total": (12936, 12936),
        "mean_dialogue_turns": (12.936, 12.936),
        "stddev_dialogue_turns": (1.6, 1.7),
        "words_total": (115000, 130000),
        "mean_dialogue_words_per_turn": (7, 10),
        "stddev_dialogue_words_per_turn": (4, 6),
        "vocab_size": (900, 1000),
        "entropy": (4, 8),
        "cond_entropy": (0.8, 2),
    },
    "task6_user": {
        "dialogues_total": (1618, 1618),
        "turns_total": (10500, 12500),
        "mean_dialogue_turns": (6, 8),
        "stddev_dialogue_turns": (2.5, 2.6),
        "words_total": (40000, 50000),
        "mean_dialogue_words_per_turn": (3, 4),
        "stddev_dialogue_words_per_turn": (2, 4),
        "vocab_size": (500, 550),
        "entropy": (4, 8),
        "cond_entropy": (0.8, 2),
    },
    "task6_system": {
        "dialogues_total": (1618, 1618),
        "turns_total": (10500, 12500),
        "mean_dialogue_turns": (6, 8),
        "stddev_dialogue_turns": (2.5, 2.6),
        "words_total": (99000, 155000),
        "mean_dialogue_words_per_turn": (9, 13),
        "stddev_dialogue_words_per_turn": (5, 9),
        "vocab_size": (520, 580),
        "entropy": (4, 8),
        "cond_entropy": (0.8, 2)
    }

}


def check_hw2_results(results):

    errors = 0
    for key in REF_RESULTS.keys():
        if key not in results:
            logger.error(f'Key {key} not present in results')
            errors += 1
            continue
        for stat in REF_RESULTS[key].keys():
            if stat not in results[key]:
                logger.error(f'Key {key}.{stat} not present in results')
                errors += 1
                continue
            lo, hi = REF_RESULTS[key][stat]
            if results[key][stat] < lo:
                logger.warning(f'{key}.{stat} seems too low ({results[key][stat]:.3f} vs. [{lo}, {hi}])')
                errors += 1
            elif results[key][stat] > hi:
                logger.warning(f'{key}.{stat} seems too high ({results[key][stat]:.3f} vs. [{lo}, {hi}])')
                errors += 1

    return errors


def check(files):

    errors = 0

    for pattern, matches in files.items():
        if pattern == 'hw2/stats.md':
            if os.path.getsize(matches[0]) < 100:
                logger.warning(f'File {pattern} is too small (<100 bytes).')
                errors += 1
        elif pattern == 'hw2/results.json':
            results = json.load(open(matches[0], 'r', encoding='UTF-8'))
            errors += check_hw2_results(results)

    return errors
