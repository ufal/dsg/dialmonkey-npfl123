
from logzero import logger
import os
import librosa
import difflib
import re
import traceback


DEADLINE = '2025-06-02'
FILES = ['hw11/tts_g2p.py',
         'hw11/test.txt',
         'hw11/test.pho',
         'hw11/test.wav', ]
CS_TEXT = 'Prof. Newton sdělil, že pizza běžně měří až 5 km. Někdo si ji dá bez oliv a artyčoků - a leckdo s dýní či kapií. Mně to svírá žaludek!'
CS_PHO = [
    ('p', 50), ('r', 50), ('o', 100), ('f', 50), ('e', 70), ('s', 50), ('o', 70), ('r', 50),
    ('n\'', 50), ('u:', 200), ('t', 50), ('n', 50),
    ('z', 50), ('d\'', 50), ('e', 100), ('l', 50), ('i', 70), ('l', 50), ('_', 200),
    ('Z', 50), ('e', 100),
    ('p', 50), ('i', 100), ('ts', 50), ('a', 70),
    ('b', 50), ('j', 50), ('e', 100), ('Z', 50), ('n\'', 50), ('e', 70),
    ('m', 50), ('n\'', 50), ('e', 100), ('r\'', 50), ('i:', 150),
    ('a', 100), ('S', 50),
    ('p', 50), ('j', 50), ('e', 100), ('t', 50),
    ('k', 50), ('i', 100), ('l', 50), ('o', 70), ('m', 50), ('e', 70), ('t', 50), ('r', 50), ('_', 500),
    ('n\'', 50), ('e', 100), ('g', 50), ('d', 50), ('o', 70),
    ('s', 50), ('i', 70),
    ('j', 50), ('i', 100),
    ('d', 50), ('a:', 200),
    ('b', 50), ('e', 100), ('z', 50), ('o', 70), ('l', 50), ('i', 70), ('f', 50),
    ('a', 100),
    ('a', 100), ('r', 50), ('t', 50), ('i', 70), ('tS', 50), ('o', 70), ('k', 50), ('u:', 150), ('_', 200),
    ('a', 100),
    ('l', 50), ('e', 100), ('dz', 50), ('g', 50), ('d', 50), ('o', 70),
    ('z', 50), ('d', 50), ('i:', 200), ('n\'', 50), ('i:', 150),
    ('tS', 50), ('i', 100),
    ('k', 50), ('a', 100), ('p', 50), ('i', 70), ('j', 50), ('i:', 150), ('_', 500),
    ('m', 50), ('n\'', 50), ('e', 70),
    ('t', 50), ('o', 70),
    ('s', 50), ('v', 50), ('i:', 200), ('r', 50), ('a:', 150),
    ('Z', 50), ('a', 100), ('l', 50), ('u', 70), ('d', 50), ('e', 70), ('k', 50), ('_', 500),
]
EN_TEXT = 'Prof. Newton stated that 235 km is a long distance. At 6 kHz, a sound wave might not be loud enough for it - if audible at all!'
EN_PHO = [
    ('p', 50), ('r', 50), ('V', 70), ('f', 50), ('e', 100), ('s', 50), ('3:', 150),
    ('n', 50), ('u:', 200), ('t', 50), ('V', 70), ('n', 50),
    ('s', 50), ('t', 50), ('eI', 200), ('t', 50), ('I', 70), ('d', 50),
    ('D', 50), ('{', 100), ('t', 50),
    ('t', 50), ('u:', 200), ('T', 50), ('r', 50), ('i:', 200), ('f', 50), ('aI', 200), ('v', 50),
    ('k', 50), ('V', 70), ('l', 50), ('A:', 200), ('m', 50), ('V', 70), ('t', 50), ('3:', 150),
    ('I', 100), ('z', 50),
    ('V', 70),
    ('l', 50), ('Q', 100), ('N', 50),
    ('d', 50), ('I', 100), ('s', 50), ('t', 50), ('V', 70), ('n', 50), ('s', 50), ('_', 500),
    ('{', 100), ('t', 50),
    ('s', 50), ('I', 100), ('k', 50), ('s', 50),
    ('k', 50), ('eI', 200), ('eI', 200), ('tS', 50), ('z', 50), ('i:', 200), ('_', 200),
    ('V', 70),
    ('s', 50), ('aU', 200), ('n', 50), ('d', 50),
    ('w', 50), ('eI', 200), ('v', 50),
    ('m', 50), ('aI', 200), ('t', 50),
    ('n', 50), ('A:', 200), ('t', 50),
    ('b', 50), ('i:', 200),
    ('l', 50), ('aU', 200), ('d', 50),
    ('I', 70), ('n', 50), ('V', 100), ('f', 50),
    ('f', 50), ('Q', 100), ('r', 50),
    ('I', 100), ('t', 50), ('_', 200),
    ('I', 100), ('f', 50),
    ('A:', 200), ('d', 50), ('V', 70), ('b', 50), ('V', 70), ('l', 50),
    ('{', 100), ('t', 50),
    ('Q', 100), ('l', 50), ('_', 500),
]


def check(files):

    errors = 0
    txt_size = 0
    pho_size = 0

    for pattern, matches in files.items():

        if pattern == 'hw11/tts_g2p.py':
            try:
                from hw11 import tts_g2p
            except Exception as e:
                traceback.print_exc()
                logger.error(f'Cannot import the g2p module, aborting: {str(e)}')
                return errors + 1

        elif pattern == 'hw11/test.txt':
            txt_size = os.path.getsize(matches[0])
            if txt_size < 20:
                logger.warning(f'File {matches[0]} is too small (<20 bytes).')
                errors += 1

        elif pattern == 'hw11/test.pho':
            phones = open(matches[0], mode='r', encoding='UTF-8').readlines()
            pho_size = len(phones)
            if pho_size < 20:
                logger.warning(f'File {matches[0]} is too small (<20 lines).')
                errors += 1

        elif pattern == 'hw11/test.wav':
            try:
                librosa.load(matches[0])
            except Exception as e:
                logger.error(f'Cannot load the {matches[0]} as a sound file: {str(e)}')
                errors += 1

    if not (0.5 < ((txt_size + 0.01) / (pho_size + 0.01)) < 2.0):
        logger.warning(f'Text and pho sizes are not aligned (ratio: {txt_size/pho_size})')
        errors += 1

    if hasattr(tts_g2p, 'g2p_czech'):
        pred_pho = tts_g2p.g2p_czech(CS_TEXT)
        ref_pho = CS_PHO
    elif hasattr(tts_g2p, 'g2p_english'):
        pred_pho = tts_g2p.g2p_english(EN_TEXT)
        ref_pho = EN_PHO
    else:
        logger.error('There\'s no G2P function implemented in tts_g2p!')
        return errors + 1

    logger.info("Predicted: " + " ".join([p + str(d) for p, d in pred_pho]))

    if pred_pho != ref_pho:
        pred_pho = [f'{p}{d}' for p, d in pred_pho]
        ref_pho = [f'{p}{d}' for p, d in ref_pho]
        diff = difflib.ndiff(ref_pho, pred_pho)
        diff = '\n'.join([d for d in diff if d[0] != '?'])
        diff = re.sub(r'([0-9]+)', r' \1', diff)
        logger.warning(f'Different pronunciation -- diff follows:\n{diff}')
        errors += 1

    return errors
