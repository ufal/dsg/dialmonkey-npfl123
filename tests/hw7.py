
import re
from itertools import groupby
from logzero import logger

from dialmonkey.conversation_handler import ConversationHandler
from dialmonkey.utils import load_conf
from dialmonkey.input.text import ListInput
from dialmonkey.da import DA
from dialmonkey.dialogue import Dialogue


DEADLINE = '2025-05-09'
FILES = ['hw7/outputs.txt',
         ('dialmonkey/policy/rule_[a-zA-Z_0-9-]+.py', 1),
         (r'conf/act_[a-zA-Z_0-9-]+.yaml', 1)]


def parse_dial(dial, parse_das=True):

    assert len(dial) % 2 == 0

    user = dial[0::2]
    sys = dial[1::2]

    assert [re.match(r'^u(se?r)?:', u, flags=re.I) for u in user]
    assert [re.match(r'^s(ys(tem)?)?:', s, flags=re.I) for s in sys]

    user = [re.sub(r'^u(se?r)?:', '', u, flags=re.I).strip() for u in user]
    sys = [re.sub(r'^s(ys(tem)?)?:', '', s, flags=re.I).strip() for s in sys]
    if parse_das:
        sys = [DA.parse_cambridge_da(s) if '&' not in s and ',' in s else DA.parse(s) for s in sys]

    return [(u, s) for u, s in zip(user, sys)]


def check(files):

    errors = 0
    string_mode = True

    for pattern, matches in files.items():

        if pattern.startswith('conf'):
            try:
                conf = load_conf(matches[0], remove_output_setting=True)
            except Exception as e:
                logger.exception(e)
                logger.error(f'Cannot load config, aborting.')
                return errors + 1

        if pattern == 'hw7/outputs.txt':
            dials = open(matches[0], mode='r', encoding='UTF-8').readlines()
            if len(dials) < 30:
                logger.warning(f'File {matches[0]} seems too short (<30 lines)')
                errors += 1
            dials = [l.strip() for l in dials]
            dials = [list(g) for k, g in groupby(dials, key=bool) if k]  # split by empty line

            try:
                dials = [parse_dial(d) for d in dials]
            except Exception as e:
                try:
                    dials = [parse_dial(d, parse_das=False) for d in dials]
                    string_mode = True
                    logger.warning(f'Cannot parse dialogues properly (likely due to quote characters), will continue in string mode.')
                    errors += 1
                except Exception as ex:
                    logger.exception(e)
                    import traceback
                    traceback.print_exc()
                    logger.error(f'Cannot parse dialogues, aborting.')
                    return errors + 1

    # run dialmonkey with the HW6/7 config file
    input_feed = ListInput()
    handler = ConversationHandler(conf, logger, user_stream=input_feed)
    for dial_no, dial in enumerate(dials, start=1):

        # run the pipeline
        input_feed.set_inputs([u for u, _ in dial])
        log = handler.run_dialogue(Dialogue())
        if not string_mode:
            preds = [DA.parse_cambridge_da(t['action']) for t in log.history]
        else:
            preds = [t['action'] for t in log.history]
        refs = [s for _, s in dial]

        for turn_no, (pred, ref) in enumerate(zip(preds, refs), start=1):
            if not string_mode:
                pred.sort()
                ref.sort()

            # check equality
            if pred != ref:
                logger.warning(f'Unexpected action at dialog {dial_no} turn {turn_no}\':\n\texpected {ref}\n\tgot      {pred}.')
                errors += 1

    return errors
