
import math
from ast import literal_eval
from logzero import logger
from dialmonkey.utils import load_conf, dynload_class
from dialmonkey.da import DA
from dialmonkey.dialogue import Dialogue
from .common import find_component


DEADLINE = '2025-04-28'
FILES = ['dialmonkey/dst/rule.py',
         (r'conf/dst_(?!test)[a-zA-Z_0-9-]+.yaml', 2),
         (r'hw5/outputs_[a-zA-Z_0-9-]+.txt', 2)]

TEST_NLUS = ['inform(food=Chinese)/0.5&inform(food=Indian)/0.2',
             'inform(food=Indian)/0.4&inform(area=center)/0.5',
             'inform(area=center)/0.7&inform(area=riverside)/0.3']
TEST_STATES = [{'food': {'Chinese': 0.5, 'Indian': 0.2, None: 0.3}},
               {'food': {'Chinese': 0.3, 'Indian': 0.52, None: 0.18}, 'area': {'center': 0.5, None: 0.5}},
               {'food': {'Chinese': 0.3, 'Indian': 0.52, None: 0.18}, 'area': {'center': 0.7, 'riverside': 0.3, None: 0.0}}]


def check_values(pred, ref):
    if sorted(list(pred.keys())) != sorted(list(ref.keys())):
        return False
    for slot in ref.keys():
        if sorted([str(k) for k in pred[slot].keys()]) != sorted([str(k) for k in ref[slot].keys()]):
            return False
        for val in ref[slot].keys():
            predicted = pred[slot].get(val)
            if predicted is None:
                predicted = pred[slot]['None']
            if not math.isclose(ref[slot][val], predicted, rel_tol=0.01):
                return False
    return True


def check(files):

    errors = 0

    for pattern, matches in files.items():

        if pattern.startswith('conf/'):
            for fname in matches:
                try:
                    conf = load_conf(fname)
                except Exception as e:
                    logger.error(f'Cannot load config from {fname}, aborting: {e}')
                    return errors + 1

                if not (find_component(conf, r'dialmonkey\.dst\.rule')):
                    logger.error(f'Didn\'t find the DST in {fname}.')
                    errors += 1

        if pattern.startswith('hw5/outputs'):
            for fname in matches:
                try:
                    data = [literal_eval(l.strip()) for l in open(fname, 'r', encoding='UTF-8').readlines()]
                except Exception as e:
                    logger.error(f'Cannot parse data in {fname}: {repr(e)}.')
                    errors += 1
                    continue

                if len(data) < 15:
                    logger.warning(f'Data in {fname} are too short.')
                    errors += 1

                for line_no, example in enumerate(data, start=1):
                    if not isinstance(example, dict):
                        logger.warning(f'Data in {fname}:{line_no} is not a dict.')
                        errors += 1
                        continue
                    for key, entry in example.items():
                        if (not isinstance(key, str)
                                or not isinstance(entry, dict)
                                or not all([(isinstance(k, str) or k is None) for k in entry.keys()])
                                or not all([isinstance(v, (float, int)) for v in entry.values()])):
                            logger.warning(f'Data in {fname}:{line_no} has strange entries.')
                            errors += 1
                            continue

    # test the DST actually:
    conf_classes = [(next(iter(c.keys())) if isinstance(c, dict) else c) for c in conf['components']]
    dst_class = dynload_class([c for c in conf_classes if c.startswith('dialmonkey.dst.rule')][0])
    dst = dst_class()

    dial = Dialogue()
    for turn_no, (nlu, state) in enumerate(zip(TEST_NLUS, TEST_STATES), start=1):
        dial.set_user_input('')
        dial.nlu = DA.parse(nlu)
        dst(dial, logger)
        if not check_values(dial.state, state):
            logger.warning(f'Test dialogue: turn {turn_no} is not equal:\n\texpected {str(state)}\n\tgot      {str(dial.state)}.')
            errors += 1
        dial.end_turn()

    return errors
