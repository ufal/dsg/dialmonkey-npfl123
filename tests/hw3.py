
import os
import re
from logzero import logger
from dialmonkey.conversation_handler import ConversationHandler
from dialmonkey.utils import load_conf
from dialmonkey.input.text import ListInput
from dialmonkey.da import DA
from dialmonkey.dialogue import Dialogue
from .common import find_component

DEADLINE = '2025-03-24'
FILES = ['hw3/README.md',
         ('dialmonkey/nlu/rule_[a-zA-Z_0-9-]+.py', 1),
         (r'conf/nlu_(?!test|dstc)[a-zA-Z_0-9-]+.yaml', 1),
         'hw3/examples.tsv']


def load_nlu_examples(filename):

    errors = 0
    examples = open(filename, mode='r', encoding='UTF-8').readlines()
    if len(examples) < 15:
        logger.warning(f'File {filename} seems too short (<15 lines)')
        errors += 1
    examples = [l for l in examples if l.strip()]  # skip empty lines

    if re.match(r'^user input\tNLU', examples[0]):  # skip header
        examples = examples[1:]
    if any(['\t' not in l for l in examples]):
        logger.error(f'Some examples in {filename} aren\'t tab-separated, aborting')
        raise Exception()

    try:
        examples = [l.strip().split('\t', 1) for l in examples]
    except Exception as e:
        logger.error(f'Cannot load examples, aborting: {e}')
        raise Exception()

    return errors, examples


def check(files):

    errors = 0

    for pattern, matches in files.items():
        if pattern.startswith('hw3/README'):
            if os.path.getsize(matches[0]) < 300:
                logger.warning(f'File {matches[0]} is too small (<300 bytes).')
                errors += 1

        if pattern.startswith('conf'):
            try:
                conf = load_conf(matches[0])
            except Exception as e:
                logger.error(f'Cannot load config, aborting: {e}')
                return errors + 1

            if not find_component(conf, r'dialmonkey\.nlu\.rule'):
                logger.error(f'Didn\'t find the NLU in config file.')
                errors += 1

        if pattern.startswith('hw3/examples'):
            try:
                load_errors, examples = load_nlu_examples(matches[0])
                errors += load_errors
            except Exception:
                return errors + 1


    # run dialmonkey with the given config file
    input_feed = ListInput()
    handler = ConversationHandler(conf, logger, user_stream=input_feed)
    for text, target_nlu in examples:
        # parse the example file
        target_nlu = DA.parse_cambridge_da(target_nlu) if '&' not in target_nlu and ',' in target_nlu else DA.parse(target_nlu)
        target_nlu.sort()

        # run the NLU
        input_feed.set_inputs([text])
        dial = handler.run_dialogue(Dialogue())
        pred_nlu = DA.parse_cambridge_da(dial.history[-1]['nlu'])
        pred_nlu.sort()

        # check equality
        if pred_nlu != target_nlu:
            logger.warning(f'Unexpected parse for `{text}\':\n\texpected {target_nlu}\n\tgot      {pred_nlu}.')
            errors += 1

    return errors
