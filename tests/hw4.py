
import os
import json
from logzero import logger
from dialmonkey.conversation_handler import ConversationHandler
from dialmonkey.utils import load_conf
from dialmonkey.input.text import ListInput
from dialmonkey.da import DA
from dialmonkey.dialogue import Dialogue
from dialmonkey.evaluation.eval_nlu import DAIFScore
from .common import find_component

DEADLINE = '2025-04-14'
FILES = ['hw4/README.md',
         'hw4/predicted.txt',
         'hw4/train_nlu.py',
         'dialmonkey/nlu/stat_dstc.py',
         'conf/nlu_dstc.yaml']


def check(files):

    errors = 0

    for pattern, matches in files.items():
        if pattern.startswith('hw4/README') or pattern.startswith('hw4/train_nlu.py'):
            if os.path.getsize(matches[0]) < 100:
                logger.warning(f'File {matches[0]} is too small (<100 bytes).')
                errors += 1

        if pattern.startswith('hw4/predicted'):
            outputs = open(matches[0], mode='r', encoding='UTF-8').readlines()
            if len(outputs) < 9890 or len(outputs) > 9891:
                logger.warning(f'File {matches[0]} has incorrect length (should be 9890 lines)')
                errors += 1
            outputs = outputs[:20]

        if pattern.startswith('conf'):
            try:
                conf = load_conf(matches[0])
            except Exception as e:
                logger.error(f'Cannot load config, aborting: {e}')
                return errors + 1

            if not find_component(conf, r'dialmonkey\.nlu\.stat_dstc'):
                logger.error(f'Didn\'t find the NLU in config file.')
                errors += 1

    # load data
    data = json.load(open(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'data', 'hw4', 'dstc2-nlu-test.json')),
                          'r', encoding='UTF-8'))
    data = data[:20]

    # run dialmonkey with the given config file
    input_feed = ListInput()
    handler = ConversationHandler(conf, logger, user_stream=input_feed)
    evaluator = DAIFScore()
    for example, output in zip(data, outputs):
        # prepare DAs & text
        output = DA.parse_cambridge_da(output.strip()) if '&' not in output and ',' in output else DA.parse(output)
        output.sort()
        target = DA.parse_cambridge_da(example['DA'])
        text = example['usr']

        # run the NLU
        input_feed.set_inputs([text])
        dial = handler.run_dialogue(Dialogue())
        pred_nlu = DA.parse_cambridge_da(dial.history[-1]['nlu'])
        pred_nlu.sort()

        # sanity check
        if pred_nlu != output:
            logger.warning(f'Unexpected output for `{text}\' (not equal to predicted.txt):\n\texpected {output}\n\tgot      {pred_nlu}.')
            errors += 1

        # proper evaluation
        evaluator.add_instances([target], [pred_nlu])

    logger.info(f'Scores on 1st 20 instances: P {evaluator.precision} R {evaluator.recall} F1 {evaluator.f1}')
    if evaluator.f1 < 0.8:
        logger.warning('F1 < 0.8')
        errors += 1
    return errors
