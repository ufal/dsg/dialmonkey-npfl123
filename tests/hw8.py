
import re
from itertools import groupby
from logzero import logger
import os

from dialmonkey.conversation_handler import ConversationHandler
from dialmonkey.utils import load_conf
from dialmonkey.input.text import ListInput
from dialmonkey.dialogue import Dialogue
from .common import find_component
from .hw7 import parse_dial


DEADLINE = '2025-05-15'
FILES = ['hw8/outputs.txt',
         ('dialmonkey/nlg/templates_[a-zA-Z_0-9-]+.py', 1),
         ('dialmonkey/nlg/templates_[a-zA-Z_0-9-]+.(yaml|json)', 1),
         (r'conf/text_[a-zA-Z_0-9-]+.yaml', 1)]

TEMPLATE_FILE = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'hw8_templates.json')  # JSON should work as subset of YAML too
TEST_INPUTS = ['hello()',
               'inform(price=expensive,area=north)',
               'inform(price=cheap)', ]
TEST_REFS = [['Hi!', 'Hello.'],
             ['This place is expensive. This place is in the north.', 'This place is in the north. This place is expensive.'],
             'You\'ll save money here.']


def compare_responses(preds, refs, dial_no=0):

    errors = 0
    for turn_no, (pred, ref) in enumerate(zip(preds, refs), start=1):
        if isinstance(ref, list):
            equal = any([pred.lower().strip() == r.lower().strip() for r in ref])
        else:
            equal = pred.lower().strip() == ref.lower().strip()

        # check equality
        if not equal:
            logger.warning(f'Unexpected response at dialog {dial_no} turn {turn_no}\':\n\texpected {ref}\n\tgot      {pred}')
            errors += 1
    return errors


def check(files):

    errors = 0

    for pattern, matches in files.items():

        if pattern.startswith('conf'):
            try:
                conf = load_conf(matches[0])
            except Exception as e:
                logger.error(f'Cannot load config, aborting: {e}')
                return errors + 1

            if not find_component(conf, r'dialmonkey\.nlg\.templates'):
                logger.error('Didn\'t find the NLG in config file.')
                errors += 1

        if pattern == 'hw8/outputs.txt':
            dials = open(matches[0], mode='r', encoding='UTF-8').readlines()
            if len(dials) < 30:
                logger.warning(f'File {matches[0]} seems too short (<30 lines)')
                errors += 1
            dials = [dial.strip() for dial in dials]
            dials = [list(g) for k, g in groupby(dials, key=bool) if k]  # split by empty line

            try:
                dials = [parse_dial(d, parse_das=False) for d in dials]
            except Exception as e:
                logger.error(f'Cannot parse dialogues, aborting: {e}')
                return errors + 1

    # run dialmonkey with the HW8 config file
    input_feed = ListInput()
    handler = ConversationHandler(conf, logger, user_stream=input_feed)
    for dial_no, dial in enumerate(dials, start=1):
        # run the pipeline
        input_feed.set_inputs([u for u, _ in dial])
        log = handler.run_dialogue(Dialogue())
        preds = [t['system'] for t in log.history]  # get system responses
        refs = [s for _, s in dial]

        errors += compare_responses(preds, refs, dial_no)

    # try loading our own templates
    conf['components'] = (['dialmonkey.policy.dummy.InputToSystemAction']
                          + [c for c in conf['components']
                             if re.match(r'dialmonkey\.nlg\.templates', next(iter(c.keys())) if isinstance(c, dict) else c)])
    input_feed = ListInput()
    handler = ConversationHandler(conf, logger, user_stream=input_feed)
    try:
        handler.components[1].load_templates(TEMPLATE_FILE)
    except Exception as e:
        logger.error(f'NLG component can\'t load templates from our file, aborting: {e}')
        return errors + 1
    input_feed.set_inputs(TEST_INPUTS)
    log = handler.run_dialogue(Dialogue())
    preds = [t['system'] for t in log.history]
    errors += compare_responses(preds, TEST_REFS)

    return errors
