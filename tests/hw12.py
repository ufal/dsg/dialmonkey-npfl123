
from logzero import logger

from dialmonkey.conversation_handler import ConversationHandler
from dialmonkey.utils import load_conf
from dialmonkey.input.text import ListInput
from dialmonkey.dialogue import Dialogue


DEADLINE = '2025-06-09'
FILES = ['hw12/samples.txt',
         'dialmonkey/policy/ir_chatbot.py',
         'conf/ir_chatbot.yaml']
DATA_FILE = 'data/hw12/dailydialog_train.txt'
TEST_INPUTS = [  # 1st 10 dialogs from DailyDialog validation set
    "Good morning , sir . Is there a bank near here ?",
    "Good afternoon . This is Michelle Li speaking , calling on behalf of IBA . Is Mr Meng available at all ?",
    "What qualifications should a reporter have ?",
    "Hi , good morning , Miss ? what can I help you with ?",
    "Excuse me , ma'am . Can you tell me where the nearest postoffice is ?",
    "Could you give me some advice on how to bring up my son properly ?",
    "I'm in 507 . I have a few problems with my room .",
    "Excuse me , sir , I'm afraid you can't park your car here .",
    "What can I do for you today ?",
    "Oh , well . It was fun to be the winner . But ... it's too big . I must be an extra small in the States .",
]


def load_dailydialog(filename):
    data = open(filename, 'r', encoding='UTF-8').readlines()
    data = [utt.strip() for dial in data for utt in dial.strip().split('__eou__')]
    data = set(data)
    return data


def check(files):

    valid_resps = load_dailydialog(DATA_FILE)
    errors = 0

    for pattern, matches in files.items():

        if pattern.startswith('conf'):
            try:
                conf = load_conf(matches[0])
            except Exception as e:
                logger.error(f'Cannot load config, aborting: {e}')
                return errors + 1

        if pattern == 'hw12/samples.txt':
            resps = open(matches[0], mode='r', encoding='UTF-8').readlines()
            if len(resps) < 10:
                logger.warning(f'File {matches[0]} seems too short (<10 lines)')
                errors += 1
            if any([resp.strip() not in valid_resps for resp in resps]):
                logger.warning(f'Some responses not in the DailyDialog training data')
                errors += 1

    # run dialmonkey with the HW12 config file
    input_feed = ListInput()
    handler = ConversationHandler(conf, logger, user_stream=input_feed)

    # run the pipeline
    input_feed.set_inputs(TEST_INPUTS)
    log = handler.run_dialogue(Dialogue())
    test_resps = [t['system'] for t in log.history]

    if any([resp.strip() not in valid_resps for resp in test_resps]):
        logger.warning(f'Some test responses not in the DailyDialog training data')
        errors += 1

    resps = set([resp.strip() for resp in resps])
    if not any([resp.strip() in resps for resp in test_resps]):
        logger.warning(f'No overlap between samples.txt and test replies')
        errors += 1

    return errors
